/**
 *
 * TextField
 *
 */

import React from 'react';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import { selectTextOnFocus } from 'utils/input';

const textField = ({
  input,
  placeholder,
  meta: { touched, error },
  ...custom
}) => (
  <TextField
    error={touched && error}
    autoFocus
    {...input}
    {...custom}
    onFocus={selectTextOnFocus}
    placeholder={placeholder}
  />
);

textField.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func,
    value: PropTypes.string,
  }),
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  placeholder: PropTypes.string,
};

export default textField;
