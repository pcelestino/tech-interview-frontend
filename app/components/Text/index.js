/**
 *
 * Text
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';

const Text = ({ variant, color, id, defaultMessage, noWrap, children }) => (
  <div>
    {children ? (
      <Typography variant={variant} color={color} noWrap={noWrap}>
        {children}
      </Typography>
    ) : (
      <Typography variant={variant} color={color} noWrap={noWrap}>
        <FormattedMessage id={id} defaultMessage={defaultMessage} />
      </Typography>
    )}
  </div>
);

Text.defaultProps = {
  variant: 'body1',
  color: 'default',
  noWrap: true,
};

Text.propTypes = {
  id: PropTypes.string,
  defaultMessage: PropTypes.string,
  variant: PropTypes.string,
  color: PropTypes.string,
  noWrap: PropTypes.bool,
  children: PropTypes.node,
};

export default Text;
