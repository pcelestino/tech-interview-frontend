/**
 *
 * AddQuestionnaireCard
 *
 */

import React from 'react';
import Text from 'components/Text';
import PropTypes from 'prop-types';
import messages from './messages';
import Card from './Card';
import Content from './Content';
import AddQuestionnaireIcon from './AddQuestionnaireIcon';

const addQuestionnaireCard = ({ to }) => (
  <Card align="center" to={to}>
    <Content>
      <Text color="textSecondary" {...messages.addQuestionnaire} />
      <AddQuestionnaireIcon />
    </Content>
  </Card>
);

addQuestionnaireCard.defaultProps = {
  to: '/',
};

addQuestionnaireCard.propTypes = {
  to: PropTypes.string,
};

export default addQuestionnaireCard;
