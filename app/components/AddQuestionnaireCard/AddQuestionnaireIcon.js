import styled from 'styled-components';
import { AddCircle } from 'styled-icons/material';

const AddQuestionnaireIcon = styled(AddCircle)`
  width: 60px;
  height: 60px;
  padding: 5px;
  color: #3f51b5;
  align-self: center;
`;

export default AddQuestionnaireIcon;
