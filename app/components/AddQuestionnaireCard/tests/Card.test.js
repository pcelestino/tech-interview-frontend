import { createShallow, createRender } from '@material-ui/core/test-utils';
import React from 'react';

import Card from '../Card';

describe('<Card />', () => {
  let shallow;
  let render;

  beforeAll(() => {
    shallow = createShallow();
    render = createRender();
  });

  it('should render an <div> tag', () => {
    const renderedComponent = render(<Card>teste</Card>);
    expect(renderedComponent[0].name).toEqual('button');
  });

  it('should have a className attribute', () => {
    const renderedComponent = shallow(<Card>teste</Card>);
    expect(renderedComponent.prop('className')).toBeDefined();
  });

  it('should adopt a valid attribute', () => {
    const id = 'test';
    const renderedComponent = render(<Card id={id}>teste</Card>);
    expect(renderedComponent.prop('id')).toEqual(id);
  });

  it('should not adopt an invalid attribute', () => {
    const renderedComponent = render(<Card color="primary">teste</Card>);
    expect(renderedComponent.prop('color')).toBeUndefined();
  });
});
