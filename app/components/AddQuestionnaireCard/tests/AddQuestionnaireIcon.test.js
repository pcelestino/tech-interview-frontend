import React from 'react';
import { shallow } from 'enzyme';

import AddQuestionnaireIcon from '../AddQuestionnaireIcon';

describe('<AddQuestionnaireIcon />', () => {
  it('should render an <AddCircle> tag', () => {
    const renderedComponent = shallow(<AddQuestionnaireIcon />);
    expect(renderedComponent.type()).toEqual('AddCircle');
  });

  it('should have a className attribute', () => {
    const renderedComponent = shallow(<AddQuestionnaireIcon />);
    expect(renderedComponent.prop('className')).toBeDefined();
  });

  it('should adopt a valid attribute', () => {
    const id = 'test';
    const renderedComponent = shallow(<AddQuestionnaireIcon id={id} />);
    expect(renderedComponent.prop('id')).toEqual(id);
  });

  it('should not adopt an invalid attribute', () => {
    const renderedComponent = shallow(
      <AddQuestionnaireIcon attribute="test" />,
    );
    expect(renderedComponent.prop('attribute')).toBeUndefined();
  });
});
