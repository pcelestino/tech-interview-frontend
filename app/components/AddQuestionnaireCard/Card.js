import React from 'react';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Card = styled(props => (
  <Button {...props} variant="contained" component={Link} />
))`
  && {
    background-color: white;
    width: 150px;
    height: 180px;
    margin: 10px;
    padding: 0;
    text-transform: none;
    display: flex;
    flex-direction: column;
    align-items: ${({ align }) => align || 'flex-start'};
    justify-content: ${({ align }) => align || 'flex-start'};
    &:hover {
      background-color: #f7f7f7;
    }
  }
`;

export default Card;
