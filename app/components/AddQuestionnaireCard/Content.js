import styled from 'styled-components';

const Content = styled.div`
  display: flex;
  flex: 1;
  border-radius: 3%;
  flex-direction: column;
  width: 100%;
  height: 100%;
`;

export default Content;
