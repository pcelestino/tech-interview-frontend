/*
 * AddQuestionnaireCard Messages
 *
 * This contains all the text for the AddQuestionnaireCard component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  addQuestionnaire: {
    id: 'app.components.AddQuestionnaireCard.add.questionnaire',
    defaultMessage: 'Novo questionário',
  },
});
