import styled from 'styled-components';
import Card from '@material-ui/core/Card';

const QuestionnaireCard = styled(Card)`
  width: 150px;
  height: 180px;
  margin: 10px;
  display: flex;
  flex-direction: column;
`;

export default QuestionnaireCard;
