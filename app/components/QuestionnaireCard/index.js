/**
 *
 * QuestionnaireCard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { ClipboardList } from 'styled-icons/fa-solid';
import { Edit, Delete } from 'styled-icons/material';
import styled from 'styled-components';
import Text from 'components/Text';
import Card from './Card';
import Header from './Header';
import Content from './Content';
import Footer from './Footer';

const AnswerQuestionnaireButton = styled(Button)`
  width: 100%;
  height: 100%;
`;

const questionnaireCard = ({
  title,
  onClickAnswer,
  onClickEdit,
  onClickDelete,
}) => (
  <Card>
    <Header>
      <Text color="textSecondary">{title}</Text>
    </Header>
    <Content>
      <AnswerQuestionnaireButton color="primary" onClick={onClickAnswer}>
        <ClipboardList size={50} />
      </AnswerQuestionnaireButton>
    </Content>
    <Footer>
      <IconButton color="primary" component="span" onClick={onClickEdit}>
        <Edit size={20} />
      </IconButton>
      <IconButton color="primary" component="span" onClick={onClickDelete}>
        <Delete size={20} />
      </IconButton>
    </Footer>
  </Card>
);

questionnaireCard.propTypes = {
  title: PropTypes.string,
  onClickAnswer: PropTypes.func,
  onClickEdit: PropTypes.func,
  onClickDelete: PropTypes.func,
};

export default questionnaireCard;
