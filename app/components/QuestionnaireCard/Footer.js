import styled from 'styled-components';

const Footer = styled.div`
  flex: 0.2;
  border-top: 0.3px rgba(0, 0, 0, 0.5) solid;
`;

export default Footer;
