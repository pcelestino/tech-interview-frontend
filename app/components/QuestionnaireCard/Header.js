import styled from 'styled-components';

const Header = styled.div`
  flex: 0.2;
  padding: 10px;
  border-bottom: 0.3px rgba(0, 0, 0, 0.5) solid;
`;

export default Header;
