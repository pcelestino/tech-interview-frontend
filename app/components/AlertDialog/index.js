/**
 *
 * AlertDialog
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import messages from './messages';

const alertDialog = ({
  title,
  description,
  open,
  onClose,
  onAccept,
  simpleResponse,
}) => (
  <Dialog open={open} onClose={onClose}>
    <DialogTitle>{title}</DialogTitle>
    <DialogContent>
      <DialogContentText>{description}</DialogContentText>
    </DialogContent>
    <DialogActions>
      {simpleResponse ? (
        <div>
          <Button onClick={onClose} color="primary">
            <FormattedMessage {...messages.ok} />
          </Button>
        </div>
      ) : (
        <div>
          <Button onClick={onClose} color="primary">
            <FormattedMessage {...messages.no} />
          </Button>
          <Button onClick={onAccept} color="primary" autoFocus>
            <FormattedMessage {...messages.yes} />
          </Button>
        </div>
      )}
    </DialogActions>
  </Dialog>
);

alertDialog.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  onAccept: PropTypes.func,
  simpleResponse: PropTypes.bool,
};

alertDialog.defaultProps = {
  simpleResponse: false,
};

export default alertDialog;
