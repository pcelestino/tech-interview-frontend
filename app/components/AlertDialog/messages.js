/*
 * AlertDialog Messages
 *
 * This contains all the text for the AlertDialog component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  yes: {
    id: 'app.components.AlertDialog.yes',
    defaultMessage: 'Sim',
  },
  no: {
    id: 'app.components.AlertDialog.no',
    defaultMessage: 'Não',
  },
  ok: {
    id: 'app.components.AlertDialog.ok',
    defaultMessage: 'Ok',
  },
});
