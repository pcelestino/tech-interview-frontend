export const selectTextOnFocus = proxy =>
  proxy.target.setSelectionRange(0, proxy.target.value.length);
