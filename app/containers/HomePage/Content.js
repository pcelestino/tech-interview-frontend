import styled from 'styled-components';

const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 80%;
  justify-content: center;
  margin: 16px;
`;

export default Content;
