/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import Text from 'components/Text';
import AddQuestionnaireCard from 'components/AddQuestionnaireCard';
import QuestionnaireCard from 'components/QuestionnaireCard';
import AlertDialog from 'components/AlertDialog';

import { makeSelectQuestionnaires } from './selectors';
import { getQuestionnaire, removeQuestionnaire } from './actions';

import reducer from './reducer';
import saga from './saga';

import messages from './messages';
import Container from './Container';
import Content from './Content';

class HomePage extends React.PureComponent {
  state = {
    openModal: false,
    questionnaireIndex: -1,
  };

  handleOpen = index => {
    this.setState({ openModal: true, questionnaireIndex: index });
  };

  handleClose = () => {
    this.setState({ openModal: false });
  };

  componentDidMount() {
    const { onPageLoaded } = this.props;
    onPageLoaded();
  }

  render() {
    const {
      questionnaires,
      onRemoveQuestionnaire,
      onClickAnswer,
      onClickEdit,
    } = this.props;
    const { openModal, questionnaireIndex } = this.state;

    return (
      <Container>
        <AppBar position="static" color="primary">
          <Toolbar>
            <Text variant="title" color="inherit" {...messages.header} />
          </Toolbar>
        </AppBar>
        <Content>
          <AddQuestionnaireCard to="/create" />

          {questionnaires.map((questionnaire, index) => (
            <QuestionnaireCard
              key={String(index)}
              onClickAnswer={() => onClickAnswer(index)}
              onClickEdit={() => onClickEdit(index)}
              onClickDelete={() => this.handleOpen(index)}
              title={questionnaire.get('questionnaire')}
            />
          ))}
        </Content>

        <AlertDialog
          title="Remover"
          description="Deseja remover o questionário"
          open={openModal}
          onClose={this.handleClose}
          onAccept={() => {
            onRemoveQuestionnaire(questionnaireIndex);
            this.handleClose();
          }}
        />
      </Container>
    );
  }
}

HomePage.propTypes = {
  questionnaires: PropTypes.object,
  onPageLoaded: PropTypes.func,
  onRemoveQuestionnaire: PropTypes.func,
  onClickAnswer: PropTypes.func,
  onClickEdit: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  questionnaires: makeSelectQuestionnaires(),
});

const mapDispatchToProps = dispatch => ({
  onPageLoaded: () => dispatch(getQuestionnaire()),
  onRemoveQuestionnaire: index => dispatch(removeQuestionnaire(index)),
  onClickAnswer: questionnaireIndex =>
    dispatch(push(`/answer/${questionnaireIndex}`)),
  onClickEdit: questionnaireIndex =>
    dispatch(push(`/create/${questionnaireIndex}`)),
});

const withReducer = injectReducer({ key: 'homePage', reducer });
const withSaga = injectSaga({ key: 'homePage', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
