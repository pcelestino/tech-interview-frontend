/*
 *
 * HomePage reducer
 *
 */

import { fromJS, List } from 'immutable';
import { STORE_QUESTIONNAIRES } from './constants';

export const initialState = fromJS({
  questionnaires: List(),
});

function createQuestionnairePageReducer(state = initialState, action) {
  switch (action.type) {
    case STORE_QUESTIONNAIRES:
      return state.set('questionnaires', fromJS(action.payload));
    default:
      return state;
  }
}

export default createQuestionnairePageReducer;
