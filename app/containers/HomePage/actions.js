/*
 *
 * HomePage actions
 *
 */

import {
  GET_QUESTIONNAIRES,
  STORE_QUESTIONNAIRES,
  REMOVE_QUESTIONNAIRE,
} from './constants';

export function getQuestionnaire() {
  return {
    type: GET_QUESTIONNAIRES,
  };
}

export function storeQuestionnaires(questionnaire) {
  return {
    type: STORE_QUESTIONNAIRES,
    payload: questionnaire,
  };
}

export function removeQuestionnaire(index) {
  return {
    type: REMOVE_QUESTIONNAIRE,
    payload: index,
  };
}
