import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the homePage state domain
 */

const selectHomePageDomain = state => state.get('homePage', initialState);

/**
 * Other specific selectors
 */

const makeSelectQuestionnaires = () =>
  createSelector(selectHomePageDomain, homePageState =>
    homePageState.get('questionnaires'),
  );

const makeSelectQuestionnaireByIndex = index =>
  createSelector(selectHomePageDomain, homePageState =>
    homePageState.get('questionnaires').get(index),
  );

export { makeSelectQuestionnaires, makeSelectQuestionnaireByIndex };
