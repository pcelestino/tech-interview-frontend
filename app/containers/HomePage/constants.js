/*
 *
 * HomePage constants
 *
 */

export const GET_QUESTIONNAIRES = 'app/HomePage/GET_QUESTIONNAIRES';
export const REMOVE_QUESTIONNAIRE = 'app/HomePage/REMOVE_QUESTIONNAIRE';
export const STORE_QUESTIONNAIRES = 'app/HomePage/STORE_QUESTIONNAIRES';
