import _ from 'lodash';
import { takeLatest, put } from 'redux-saga/effects';
import { GET_QUESTIONNAIRES, REMOVE_QUESTIONNAIRE } from './constants';
import { QUESTIONNAIRES } from '../App/constants';
import { storeQuestionnaires } from './actions';

export function* getQuestionnaires() {
  // Get and store questionnaires on redux store
  if (localStorage.getItem(QUESTIONNAIRES)) {
    const questionnaires = yield JSON.parse(
      localStorage.getItem(QUESTIONNAIRES),
    );
    yield put(storeQuestionnaires(questionnaires));
  }
}

export function* removeQuestionnaire({ payload: questionnaireIndex }) {
  // Remove questionnaire from localStore and redux store
  if (localStorage.getItem(QUESTIONNAIRES)) {
    const questionnaires = yield JSON.parse(
      localStorage.getItem(QUESTIONNAIRES),
    );

    const filteredQuestionnaires = yield _.filter(
      questionnaires,
      (questionnaire, index) => index !== questionnaireIndex && questionnaire,
    );

    yield localStorage.setItem(
      QUESTIONNAIRES,
      JSON.stringify(filteredQuestionnaires),
    );

    yield put(storeQuestionnaires(filteredQuestionnaires));
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  yield takeLatest(GET_QUESTIONNAIRES, getQuestionnaires);
  yield takeLatest(REMOVE_QUESTIONNAIRE, removeQuestionnaire);
}
