/*
 *
 * CreateQuestionnairePage actions
 *
 */

import {
  CLOSE_ALERT_MODAL,
  EDIT_QUESTIONNAIRE,
  SAVE_QUESTIONNAIRE,
  SHOW_ALERT_MODAL,
} from './constants';

export function saveQuestionnaire(immutableQuestionnaire) {
  return {
    type: SAVE_QUESTIONNAIRE,
    payload: immutableQuestionnaire,
  };
}

export function editQuestionnaire(immutableQuestionnaire, index) {
  return {
    type: EDIT_QUESTIONNAIRE,
    payload: { immutableQuestionnaire, index },
  };
}

export function showAlertModalAction({ title, description }) {
  return {
    type: SHOW_ALERT_MODAL,
    payload: { title, description },
  };
}

export function closeAlertModal() {
  return {
    type: CLOSE_ALERT_MODAL,
  };
}
