/**
 *
 * CreateQuestionnairePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import AlertDialog from 'components/AlertDialog';

import makeSelectCreateQuestionnairePage, {
  makeSelectShowAlertModal,
  makeSelectAlertModalTitle,
  makeSelectAlertModalDescription,
} from './selectors';
import { makeSelectQuestionnaireByIndex } from '../HomePage/selectors';

import reducer from './reducer';
import saga from './saga';
import HeaderForm from './HeaderForm';
import Form from './Form';

import {
  saveQuestionnaire,
  editQuestionnaire,
  closeAlertModal,
} from './actions';
import { validation } from './validations';

export class CreateQuestionnairePage extends React.PureComponent {
  render() {
    const {
      onGoBack,
      onSubmit,
      initialValues,
      showAlertModal,
      onCloseAlertModal,
      alertModalTitle,
      alertModalDescription,
    } = this.props;
    return (
      <div>
        <HeaderForm
          goBack={onGoBack}
          onSubmit={onSubmit}
          initialValues={initialValues}
        />
        <Form />
        <AlertDialog
          title={alertModalTitle}
          description={alertModalDescription}
          open={showAlertModal}
          onClose={onCloseAlertModal}
          simpleResponse
        />
      </div>
    );
  }
}

CreateQuestionnairePage.propTypes = {
  onGoBack: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  initialValues: PropTypes.object,
  showAlertModal: PropTypes.bool,
  onCloseAlertModal: PropTypes.func,
  alertModalTitle: PropTypes.string,
  alertModalDescription: PropTypes.string,
};

const mapStateToProps = (
  state,
  {
    match: {
      params: { questionnaireIndex },
    },
  },
) =>
  createStructuredSelector({
    createquestionnairepage: makeSelectCreateQuestionnairePage(),
    initialValues: makeSelectQuestionnaireByIndex(questionnaireIndex),
    showAlertModal: makeSelectShowAlertModal(),
    alertModalTitle: makeSelectAlertModalTitle(),
    alertModalDescription: makeSelectAlertModalDescription(),
  });

const mapDispatchToProps = (
  dispatch,
  {
    match: {
      params: { questionnaireIndex },
    },
  },
) => ({
  onGoBack: () => dispatch(goBack()),
  onCloseAlertModal: () => dispatch(closeAlertModal()),
  onSubmit: values => {
    if (validation(dispatch, values)) {
      if (!questionnaireIndex) {
        dispatch(saveQuestionnaire(values));
      } else {
        dispatch(editQuestionnaire(values, parseInt(questionnaireIndex, 10)));
      }
      dispatch(goBack());
    }
  },
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'createQuestionnairePage', reducer });
const withSaga = injectSaga({ key: 'createQuestionnairePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(CreateQuestionnairePage);
