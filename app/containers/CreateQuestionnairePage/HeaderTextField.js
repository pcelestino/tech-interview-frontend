import React from 'react';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledTextField = styled(TextField)`
  width: 80%;
`;

const headerTextField = ({
  input,
  placeholder,
  meta: { touched, error },
  ...custom
}) => (
  <StyledTextField
    error={touched && error}
    autoFocus
    {...input}
    {...custom}
    placeholder={placeholder}
  />
);

headerTextField.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func,
    value: PropTypes.string,
  }),
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  placeholder: PropTypes.string,
};

export default headerTextField;
