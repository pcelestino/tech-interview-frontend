import React from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Field, reduxForm } from 'redux-form/immutable';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { Save, ArrowBack } from 'styled-icons/material';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { MuiThemeProvider } from '@material-ui/core/styles';
import HeaderTextField from './HeaderTextField';
import { themeDark } from '../../theme';

const StyledToolbar = styled(Toolbar)`
  flex: 1;
  display: flex;
  justify-content: space-between;
`;

const headerForm = ({ goBack, handleSubmit }) => (
  <MuiThemeProvider theme={themeDark}>
    <form onSubmit={handleSubmit}>
      <Helmet>
        <title>Criação de Questionários</title>
        <meta name="description" content="Cria um questionário" />
      </Helmet>
      <AppBar position="static" color="primary">
        <StyledToolbar>
          <IconButton color="inherit" onClick={goBack}>
            <ArrowBack size={30} />
          </IconButton>
          <Field
            name="questionnaire"
            component={HeaderTextField}
            InputProps={{ style: { fontSize: '1.3125rem' } }}
            placeholder="Questionário sem título"
          />
          <IconButton color="inherit" onClick={handleSubmit}>
            <Save size={30} />
          </IconButton>
        </StyledToolbar>
      </AppBar>
    </form>
  </MuiThemeProvider>
);

headerForm.propTypes = {
  goBack: PropTypes.func,
  handleSubmit: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
});

const createQuestionnaireHeaderForm = reduxForm({
  form: 'CreateQuestionnaireForm',
})(headerForm);

export default connect(mapStateToProps)(createQuestionnaireHeaderForm);
