import React from 'react';
import { connect } from 'react-redux';
import { FieldArray, reduxForm } from 'redux-form/immutable';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { makeSelectInitialValuesForm } from './selectors';
import Questions from './Questions';

const createQuestionnaireForm = ({ dispatch }) => (
  <form>
    <FieldArray dispatch={dispatch} name="questions" component={Questions} />
  </form>
);

createQuestionnaireForm.propTypes = {
  dispatch: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  initialValues: makeSelectInitialValuesForm(),
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const createQuestionnaireReduxForm = reduxForm({
  form: 'CreateQuestionnaireForm',
})(createQuestionnaireForm);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(createQuestionnaireReduxForm);
