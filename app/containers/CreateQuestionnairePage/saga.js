import { takeLatest } from 'redux-saga/effects';
import { EDIT_QUESTIONNAIRE, SAVE_QUESTIONNAIRE } from './constants';
import { QUESTIONNAIRES } from '../App/constants';

export function* saveQuestionnaire(action) {
  const questionnaire = yield action.payload.toJS();

  // Save the questionnaire on localStorage
  if (localStorage.getItem(QUESTIONNAIRES)) {
    const questionnaires = yield JSON.parse(
      localStorage.getItem(QUESTIONNAIRES),
    );
    yield localStorage.setItem(
      QUESTIONNAIRES,
      JSON.stringify([...questionnaires, questionnaire]),
    );
  } else {
    yield localStorage.setItem(QUESTIONNAIRES, JSON.stringify([questionnaire]));
  }
}

export function* editQuestionnaire(action) {
  const { immutableQuestionnaire, index } = action.payload;
  const questionnaire = yield immutableQuestionnaire.toJS();

  // Edit and save the questionnaire on localStorage
  if (localStorage.getItem(QUESTIONNAIRES)) {
    const questionnaires = yield JSON.parse(
      localStorage.getItem(QUESTIONNAIRES),
    );
    yield localStorage.setItem(
      QUESTIONNAIRES,
      JSON.stringify([
        ...questionnaires.slice(0, index),
        questionnaire,
        ...questionnaires.slice(index + 1),
      ]),
    );
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  yield takeLatest(SAVE_QUESTIONNAIRE, saveQuestionnaire);
  yield takeLatest(EDIT_QUESTIONNAIRE, editQuestionnaire);
}
