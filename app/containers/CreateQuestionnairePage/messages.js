/*
 * CreateQuestionnairePage Messages
 *
 * This contains all the text for the CreateQuestionnairePage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.CreateQuestionnairePage.header',
    defaultMessage: 'This is CreateQuestionnairePage container !',
  },
});
