import _ from 'lodash';
import { showAlertModalAction } from './actions';

export const validation = (dispatch, values) => {
  const { questionnaire, questions } = values.toJS();
  let isValid = true;

  if (!questionnaire || questionnaire.trim() === '') {
    dispatch(
      showAlertModalAction({
        title: 'O questionário contém erros',
        description: 'É necessário um título para o questionário',
      }),
    );
    isValid = false;
  }

  if (!questions || questions.length === 0) {
    dispatch(
      showAlertModalAction({
        title: 'O questionário contém erros',
        description: 'É necessário ao menos uma questão',
      }),
    );
    isValid = false;
  }

  _.forEach(questions, question => {
    const { correctOption, questionTitle, question: options } = question;
    if (correctOption === undefined) {
      dispatch(
        showAlertModalAction({
          title: 'O questionário contém erros',
          description: 'É necessário escolher uma questão correta',
        }),
      );
      isValid = false;
    }

    if (!questionTitle || questionTitle.trim() === '') {
      dispatch(
        showAlertModalAction({
          title: 'O questionário contém erros',
          description: 'É necessário um título para a questão',
        }),
      );
      isValid = false;
    }

    if (!options || options.length < 4) {
      dispatch(
        showAlertModalAction({
          title: 'O questionário contém erros',
          description: 'É necessário no mínimo quatro opções para a questão',
        }),
      );
      isValid = false;
    }
  });

  return isValid;
};
