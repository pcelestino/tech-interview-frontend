/**
 *
 * Asynchronously loads the component for CreateQuestionnairePage
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
