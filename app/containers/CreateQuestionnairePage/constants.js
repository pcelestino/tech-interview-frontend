/*
 *
 * CreateQuestionnairePage constants
 *
 */

export const SAVE_QUESTIONNAIRE =
  'app/CreateQuestionnairePage/SAVE_QUESTIONNAIRE';
export const EDIT_QUESTIONNAIRE =
  'app/CreateQuestionnairePage/EDIT_QUESTIONNAIRE';
export const SHOW_ALERT_MODAL = 'app/CreateQuestionnairePage/SHOW_ALERT_MODAL';
export const CLOSE_ALERT_MODAL =
  'app/CreateQuestionnairePage/CLOSE_ALERT_MODAL';
