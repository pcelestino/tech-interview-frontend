/*
 *
 * CreateQuestionnairePage reducer
 *
 */

import { fromJS, Map } from 'immutable';
import { CLOSE_ALERT_MODAL, SHOW_ALERT_MODAL } from './constants';

export const initialState = fromJS({
  initialValues: {
    questions: [Map()],
  },
  showAlertModal: false,
  alertModalTitle: '',
  alertModalDescription: '',
});

function createQuestionnairePageReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_ALERT_MODAL:
      return state
        .set('showAlertModal', true)
        .set('alertModalTitle', action.payload.title)
        .set('alertModalDescription', action.payload.description);
    case CLOSE_ALERT_MODAL:
      return state.set('showAlertModal', false);
    default:
      return state;
  }
}

export default createQuestionnairePageReducer;
