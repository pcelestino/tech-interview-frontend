import React from 'react';
import Button from '@material-ui/core/Button';
import { Add, Delete } from 'styled-icons/material';
import { Map } from 'immutable';
import { Field, FieldArray, change } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import styled from 'styled-components';
import IconButton from '@material-ui/core/IconButton';
import TextField from 'components/TextField';
import Question from './Question';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-bottom: 50px;
`;

const AddQuestionButton = styled(Button)`
  top: 20px;
`;

const QuestionTitleContainer = styled.div`
  padding: 20px 20px 0 40px;
`;

const QuestionsContainer = styled.div`
  padding-left: 25px;
`;

const DeleteQuestionButtonContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: flex-end;
  margin-left: 40px;
  border-top: 0.3px rgba(0, 0, 0, 0.5) solid;
`;

const StyledCard = styled(Card)`
  margin: 15px;
  width: 80%;
  max-width: 770px;
`;

const questions = ({ fields, dispatch }) => (
  <Container>
    {fields.map((member, index) => (
      <StyledCard key={String(index)}>
        <QuestionTitleContainer>
          <Field
            name={`${member}.questionTitle`}
            component={TextField}
            InputProps={{ style: { fontSize: '20px', minWidth: '350px' } }}
            placeholder="Pergunta"
          />
        </QuestionTitleContainer>
        <QuestionsContainer>
          <FieldArray
            dispatch={dispatch}
            name={`${member}.question`}
            component={Question}
            correctOptionId={`${member}.correctOption`}
            onCheckedCorrectOption={optIndex => {
              dispatch(
                change(
                  'CreateQuestionnaireForm',
                  `${member}.correctOption`,
                  optIndex,
                ),
              );
            }}
          />
        </QuestionsContainer>
        <DeleteQuestionButtonContainer>
          <IconButton color="default" onClick={() => fields.remove(index)}>
            <Delete size={25} />
          </IconButton>
        </DeleteQuestionButtonContainer>
      </StyledCard>
    ))}
    <AddQuestionButton
      variant="fab"
      color="primary"
      onClick={() => fields.push(Map())}
    >
      <Add size={30} />
    </AddQuestionButton>
  </Container>
);

questions.propTypes = {
  fields: PropTypes.object,
  dispatch: PropTypes.func,
};

export default questions;
