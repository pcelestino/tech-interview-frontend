import { fromJS } from 'immutable';
import createQuestionnairePageReducer from '../reducer';

describe('createQuestionnairePageReducer', () => {
  it('returns the initial state', () => {
    expect(createQuestionnairePageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
