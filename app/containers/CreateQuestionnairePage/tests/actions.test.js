import { saveQuestionnaire } from '../actions';
import { SAVE_QUESTIONNAIRE } from '../constants';

describe('CreateQuestionnairePage actions', () => {
  describe('Default Action', () => {
    it('has a type of SAVE_QUESTIONNAIRE', () => {
      const expected = {
        type: SAVE_QUESTIONNAIRE,
      };
      expect(saveQuestionnaire()).toEqual(expected);
    });
  });
});
