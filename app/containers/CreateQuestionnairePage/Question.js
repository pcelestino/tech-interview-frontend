import React from 'react';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';
import { Field, formValueSelector } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';
import Radio from '@material-ui/core/Radio';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import styled from 'styled-components';
import { Clear } from 'styled-icons/material';
import RadioTextField from 'components/TextField';

const themeError = createMuiTheme({
  palette: {
    primary: red,
  },
});

const themeCorrect = createMuiTheme({
  palette: {
    primary: green,
  },
});

const AddOptionContainer = styled.div`
  display: block;
  margin-bottom: 20px;
`;

const RemoveOptionButton = styled(IconButton)`
  && {
    display: none;
  }
`;

const OptionContainer = styled.div`
  margin: 10px 10px 10px 0;
  &:hover ${RemoveOptionButton} {
    display: inline;
  }
`;

const addOption = fields =>
  fields.push(fromJS({ option: `Opção ${fields.length + 1}` }));

const removeOption = (fields, index) => fields.remove(index);

const question = ({ fields, onCheckedCorrectOption, correctOption }) => (
  <div>
    {fields.map((member, index) => (
      <MuiThemeProvider
        theme={correctOption === index ? themeCorrect : themeError}
        key={String(index)}
      >
        <OptionContainer>
          <Radio
            color="primary"
            checked={correctOption === index}
            onChange={() => onCheckedCorrectOption(index)}
          />
          <Field
            name={`${member}.option`}
            component={RadioTextField}
            placeholder="Pergunta"
          />
          <RemoveOptionButton
            color="default"
            onClick={() => removeOption(fields, index)}
          >
            <Clear size={25} />
          </RemoveOptionButton>
        </OptionContainer>
      </MuiThemeProvider>
    ))}
    <AddOptionContainer>
      <Radio checked={false} onClick={() => addOption(fields)} />
      <TextField
        placeholder="Adicionar opção"
        onClick={() => addOption(fields)}
      />
    </AddOptionContainer>
  </div>
);

question.propTypes = {
  fields: PropTypes.object,
  onCheckedCorrectOption: PropTypes.func,
  correctOption: PropTypes.number,
};

const selector = formValueSelector('CreateQuestionnaireForm');
const mapStateToProps = (state, ownProps) => ({
  correctOption: selector(state, ownProps.correctOptionId),
});

export default connect(mapStateToProps)(question);
