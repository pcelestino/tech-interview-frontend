import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the createQuestionnairePage state domain
 */

const selectCreateQuestionnairePageDomain = state =>
  state.get('createQuestionnairePage', initialState);

/**
 * Other specific selectors
 */

const makeSelectInitialValuesForm = () =>
  createSelector(
    selectCreateQuestionnairePageDomain,
    createQuestionnairePageState =>
      createQuestionnairePageState.get('initialValues'),
  );

const makeSelectShowAlertModal = () =>
  createSelector(
    selectCreateQuestionnairePageDomain,
    createQuestionnairePageState =>
      createQuestionnairePageState.get('showAlertModal'),
  );

const makeSelectAlertModalTitle = () =>
  createSelector(
    selectCreateQuestionnairePageDomain,
    createQuestionnairePageState =>
      createQuestionnairePageState.get('alertModalTitle'),
  );

const makeSelectAlertModalDescription = () =>
  createSelector(
    selectCreateQuestionnairePageDomain,
    createQuestionnairePageState =>
      createQuestionnairePageState.get('alertModalDescription'),
  );

/**
 * Default selector used by CreateQuestionnairePage
 */

const makeSelectCreateQuestionnairePage = () =>
  createSelector(selectCreateQuestionnairePageDomain, substate =>
    substate.toJS(),
  );

export default makeSelectCreateQuestionnairePage;
export {
  selectCreateQuestionnairePageDomain,
  makeSelectInitialValuesForm,
  makeSelectShowAlertModal,
  makeSelectAlertModalTitle,
  makeSelectAlertModalDescription,
};
