import React from 'react';
import { connect } from 'react-redux';
import { Field, formValueSelector } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';
import Radio from '@material-ui/core/Radio';
import styled from 'styled-components';
import Label from './Label';

const themeError = createMuiTheme({
  palette: {
    primary: red,
  },
});

const themeCorrect = createMuiTheme({
  palette: {
    primary: green,
  },
});

const OptionContainer = styled.div`
  margin: 10px 10px 10px 0;
  display: flex;
  align-items: center;
  pointer-events: ${({ disable }) => disable && 'none'};
`;

const question = ({
  fields,
  onCheckedCorrectOption,
  correctOption,
  optionAnswered,
}) => (
  <div>
    {fields.map((member, index) => (
      <MuiThemeProvider
        theme={correctOption === index ? themeCorrect : themeError}
        key={String(index)}
      >
        <OptionContainer disable={optionAnswered !== undefined}>
          <Radio
            color="primary"
            checked={
              optionAnswered !== undefined
                ? correctOption === index || optionAnswered === index
                : false
            }
            onChange={() => onCheckedCorrectOption(index)}
          />
          <Field
            name={`${member}.option`}
            component={Label}
            placeholder="Pergunta"
            onClick={() => onCheckedCorrectOption(index)}
          />
        </OptionContainer>
      </MuiThemeProvider>
    ))}
  </div>
);

question.propTypes = {
  fields: PropTypes.object,
  onCheckedCorrectOption: PropTypes.func,
  correctOption: PropTypes.number,
  optionAnswered: PropTypes.number,
};

const selector = formValueSelector('CreateQuestionnaireForm');
const mapStateToProps = (state, ownProps) => ({
  correctOption: selector(state, ownProps.correctOptionId),
  optionAnswered: selector(state, ownProps.optionAnsweredId),
});

export default connect(mapStateToProps)(question);
