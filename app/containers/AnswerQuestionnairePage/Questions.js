import React from 'react';
import { Field, FieldArray, change } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import styled from 'styled-components';
import Question from './Question';
import Label from './Label';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-bottom: 50px;
`;

const QuestionTitleContainer = styled.div`
  padding: 20px 20px 0 40px;
`;

const QuestionsContainer = styled.div`
  padding-left: 25px;
`;

const StyledCard = styled(Card)`
  margin: 15px;
  width: 80%;
  max-width: 770px;
  padding-bottom: 5px;
`;

const questions = ({ fields, dispatch }) => (
  <Container>
    {fields.map((member, index) => (
      <StyledCard key={String(index)}>
        <QuestionTitleContainer>
          <Field
            name={`${member}.questionTitle`}
            component={Label}
            variant="headline"
            placeholder="Pergunta"
          />
        </QuestionTitleContainer>
        <QuestionsContainer>
          <FieldArray
            dispatch={dispatch}
            name={`${member}.question`}
            component={Question}
            correctOptionId={`${member}.correctOption`}
            optionAnsweredId={`${member}.optionAnswered`}
            onCheckedCorrectOption={optIndex => {
              dispatch(
                change(
                  'CreateQuestionnaireForm',
                  `${member}.optionAnswered`,
                  optIndex,
                ),
              );
            }}
          />
        </QuestionsContainer>
      </StyledCard>
    ))}
  </Container>
);

questions.propTypes = {
  fields: PropTypes.object,
  dispatch: PropTypes.func,
};

export default questions;
