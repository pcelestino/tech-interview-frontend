import React from 'react';
import { connect } from 'react-redux';
import { FieldArray, reduxForm } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import Questions from './Questions';

const createQuestionnaireForm = ({ dispatch }) => (
  <form>
    <FieldArray dispatch={dispatch} name="questions" component={Questions} />
  </form>
);

createQuestionnaireForm.propTypes = {
  dispatch: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const createQuestionnaireReduxForm = reduxForm({
  form: 'CreateQuestionnaireForm',
})(createQuestionnaireForm);

export default connect(
  null,
  mapDispatchToProps,
)(createQuestionnaireReduxForm);
