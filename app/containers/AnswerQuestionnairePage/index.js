/**
 *
 * AnswerQuestionnairePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectAnswerQuestionnairePage from './selectors';
import { makeSelectQuestionnaireByIndex } from '../HomePage/selectors';
import reducer from './reducer';
import saga from './saga';
import HeaderForm from './HeaderForm';
import Form from './Form';

/* eslint-disable react/prefer-stateless-function */
export class AnswerQuestionnairePage extends React.PureComponent {
  render() {
    const { onGoBack, onSubmit, initialValues } = this.props;
    return (
      <div>
        <HeaderForm
          goBack={onGoBack}
          onSubmit={onSubmit}
          initialValues={initialValues}
        />
        <Form />
      </div>
    );
  }
}

AnswerQuestionnairePage.propTypes = {
  onGoBack: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  initialValues: PropTypes.object,
};

const mapStateToProps = (
  state,
  {
    match: {
      params: { questionnaireIndex },
    },
  },
) =>
  createStructuredSelector({
    answerquestionnairepage: makeSelectAnswerQuestionnairePage(),
    initialValues: makeSelectQuestionnaireByIndex(questionnaireIndex),
  });

const mapDispatchToProps = dispatch => ({
  onGoBack: () => dispatch(goBack()),
  onSubmit: () => dispatch(goBack()),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'answerQuestionnairePage', reducer });
const withSaga = injectSaga({ key: 'answerQuestionnairePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AnswerQuestionnairePage);
