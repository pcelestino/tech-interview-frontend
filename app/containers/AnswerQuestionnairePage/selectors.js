import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the answerQuestionnairePage state domain
 */

const selectAnswerQuestionnairePageDomain = state =>
  state.get('answerQuestionnairePage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by AnswerQuestionnairePage
 */

const makeSelectAnswerQuestionnairePage = () =>
  createSelector(selectAnswerQuestionnairePageDomain, substate =>
    substate.toJS(),
  );

export default makeSelectAnswerQuestionnairePage;
export { selectAnswerQuestionnairePageDomain };
