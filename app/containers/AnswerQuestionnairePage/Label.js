import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

const label = ({ input, ...custom }) => (
  <Typography {...input} {...custom}>
    {input.value}
  </Typography>
);

label.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func,
    value: PropTypes.string,
  }),
};

export default label;
