import { fromJS } from 'immutable';
import answerQuestionnairePageReducer from '../reducer';

describe('answerQuestionnairePageReducer', () => {
  it('returns the initial state', () => {
    expect(answerQuestionnairePageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
