import { createMuiTheme } from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';
import pink from '@material-ui/core/colors/pink';
import red from '@material-ui/core/colors/red';

const defaultPalette = {
  primary: indigo,
  secondary: pink,
  error: red,
  // Used by `getContrastText()` to maximize the contrast between the
  // background and the text.
  contrastThreshold: 3,
  // Used to shift a color's luminance by approximately
  // two indexes within its tonal palette.
  // E.g., shift from Red 500 to Red 300 or Red 700.
  tonalOffset: 0.2,
};

// Create a light theme to provide
const themeLight = createMuiTheme({
  palette: { ...defaultPalette },
});

// Create a dark theme to provide
const themeDark = createMuiTheme({
  palette: {
    ...defaultPalette,
    type: 'dark',
  },
});

export { themeLight, themeDark };
