# Tech Interview Frontend

Project to create questionnaires and answer them

## Getting Started

Clone the project and run the yarn or npm install command and them yarn start or npm run start to execute development server

## Running the tests

Execute with the command yarn test

### And coding style tests

Execute with the command yarn lint or npm run lint

## Authors

* **Pedro Celestino** - *Initial work* - [pcelestino](https://github.com/pcelestino)

## License

This project is licensed under the MIT License

## Acknowledgments

* React Boilerplate
